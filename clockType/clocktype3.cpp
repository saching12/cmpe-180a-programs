#include<fstream>
#include<vector>
#include<string>
#include<iostream>
#include<sstream>
#include<iomanip>
using namespace std;

class clockType{
    
    
    public:
        clockType(int hours, int minutes, int seconds);
    
        void setTime (int hours, int minutes, int seconds);
        //Function to set the time
        // The time is set according to the parameters
        //Postcondition: hr = hours; min = minutes; sec = seconds
        // The function checks whether the values of hours,
        //minutes, and seconds are valid. If a value is invalid,
        //the default value 0 is assigned.
    
        void getTime (int& hours, int& minutes, int& seconds) const;
        //Function to return the tine
        //Postcondition: hours = hr; minutes = min; seconds = sec
        void printTime() const;
        //Function to print the time
        // Postcondition: Time is printed in the form hh:mm:ss.
        void incrementSeconds ();
        //Function to increment the time by one second
        //Postcondition: The time is incremented by one second.
        // If the before-increment time is 23:59:59, the time
        // is reset to 00:00:00.
        void incrementMinutes ();
    
        void incrementHours ();
        //Function to increment the time by one hour
        //Postcondition: The time is incremented by one hour.
        //If the before-increment time is 23:45:53, the time
        // is reset to 00:45:53.
    
        bool equalTime (const clockType& otherclock) const;
        //Function to compare the two times
        //Postcondition: Returns true if this time is equal to
        // otherClock; otherwise, returns false
    
    private:
        int hr; //stores the hours
        int min; //store the minutes
        int sec; //store the seconds };

};

clockType::clockType(int hours, int minutes, int seconds){
    
    setTime(hours, minutes, seconds);

}

void clockType::setTime(int hours, int minutes, int seconds){
    
    if(hours > 23 ){
        hr = 0;
    }
    else{
        hr = hours;
        min = minutes;
        sec = seconds;
    }
    
    if(minutes > 59 ){
        min = 0;
    }
    else{
        min = minutes;
    }
    
    if(seconds > 59 ){
        sec = 0;
    }
    else{
        sec = seconds;
    }
}


void clockType::getTime (int& hours, int& minutes, int& seconds) const
{
    hours = hr;
    minutes=min;
    seconds = sec;
}

void clockType::printTime() const{
    cout << setfill('0') << setw(2) << hr << ':' << setw(2) << min << ':' << setw(2) << sec << endl;
}

void clockType::incrementSeconds (){
    sec++;
    if (sec > 59)
    {
        sec = sec - 60;
        min++;
        if (min > 59)
        {
            min = min - 60;
            hr++;
            if(hr >23)
                hr = hr - 24;
        }
        
    }
}

void clockType::incrementMinutes (){
    min++;
    if (min > 59)
    {
        min = min - 60;
        hr++;
        if(hr >23)
            hr = hr - 24;
    }
}


void clockType::incrementHours (){
    hr++;
    if(hr >23)
        hr = hr - 24;
}


bool clockType::equalTime (const clockType& otherclock) const{
    
    int O_hr,O_min, O_sec;
    otherclock.getTime(O_hr, O_min, O_sec);
    
    if((hr != O_hr) || (min != O_min) || (sec != O_sec))
         return false;
    else
        return true;
}





int main(){
    
    int hr;
    int min;
    int sec;
    bool is_time_same;
    const clockType present_time(2,19,22);
    const clockType old_time(12,15,32);

    present_time.printTime();
    present_time.setTime(hr,min,sec);
    present_time.incrementHours();
    present_time.incrementSeconds();
    present_time.incrementMinutes();
    present_time.getTime(hr,min,sec);
    is_time_same = present_time.equalTime(old_time);

    cout << setfill('0') << setw(2) << hr << ':' << setw(2) << min << ':' << setw(2) << sec << endl;
    if (is_time_same)
        cout<<"time is same" << endl;
    else
        cout << "time is different"<<endl;
    
    
}
