#include<fstream>
#include<vector>
#include<string>
#include<iostream>
#include<sstream>
using namespace std;

void load_data(vector<vector<int>>& in, string file){
    ifstream iostream;
    int next;
    string line;
    vector<int> temp;
    
    iostream.open(file);
    if(iostream.fail())
    {
         cout << "\n input file Not found or unable to read"<<endl;
        exit(0);
    }
    while(getline(iostream,line)){
        if(line.empty())
            continue;
        
        
            stringstream row(line);
        
            while(row >> next)
            {
                temp.push_back(next);
                if (row.peek() == ',' || row.peek() == ' ' || row.peek() == '\t')
                    row.ignore();
            }
            in.push_back(temp);
            temp.clear();
        
    }
    iostream.close();
    
}

void matrix_multiplier(vector<vector<int>>& in1, vector<vector<int>>& in2, vector<vector<int>>& out)
{
    vector<int> temp;
    for(int i = 0;i < in1.size(); i++){
        for(int j = 0;j < in2[0].size(); j++){
            int sum = 0;
            for(int k = 0;k < in1[0].size(); k++){
                sum += in1[i][k] * in2[k][j];
            }
            temp.push_back(sum);
        }
        out.push_back(temp);
        temp.clear();
    }
}


int main(int argc, char** argv){
    
    vector<vector<int>> mat1,mat2,output_matrix;
    
    
    load_data(mat1, argv[2]);

    
    load_data(mat2, argv[4]);
    
     if(mat1[0].size() == mat2.size()){
         matrix_multiplier(mat1,mat2,output_matrix);
         
         for(int i = 0;i < output_matrix.size(); i++){
             for(int j = 0;j < output_matrix[0].size(); j++){
                 cout << output_matrix[i][j] << " ";
             }
             cout << endl;
         }
         
     }
     else
         cout<< "matrix multiplication is not possible since column of the first matrix is \
         not equal to the row of second matrix or it contains a character" <<endl;
    
    return 0;
    
}




