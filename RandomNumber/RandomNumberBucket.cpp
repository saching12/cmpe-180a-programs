//gid-25
#include<vector>
#include<iostream>
#include <stdlib.h>
#include "RandomNumberBucket.h"
using namespace std;


RandomNumberBucket::RandomNumberBucket(){
    
    max_number = 39;
    srandom(time(0));
    
}

RandomNumberBucket::RandomNumberBucket(int range){
    
    max_number = range;
    srandom(time(0));
    
}

int RandomNumberBucket::popWithRefill(){
    int pop_number = pop();
    if(empty())
        refill();
    

    return pop_number;
    
}

int RandomNumberBucket::pop(){
    int popped_number;
    int check = 0;
    while (1) {
        popped_number = (random() % max_number) + 1;
        check = check_repeat(popped_number);
        if (check != 0) {
            break;
        }
    }
    return check;
    
}


void RandomNumberBucket::refill(){

    Flushed_list.clear();
    
}


bool RandomNumberBucket::empty(){
    
    if(Flushed_list.size() == max_number)
        return true;
    else
        return false;
    
}


int RandomNumberBucket::size(){
    
    return (max_number - Flushed_list.size());
    
}


int RandomNumberBucket::check_repeat(int check_number){
    bool is_found = false;

   
    if(Flushed_list.empty()){

        Flushed_list.push_back(check_number);
        return check_number;
    }
    else if(Flushed_list.size() == max_number){

        return -1;
    }
    else{
        for(int i = 0; i<Flushed_list.size(); i++){
            if(Flushed_list[i]==check_number)
            {
                is_found = true;
                break;

            }
            
        }
        if(is_found)
            return 0;
        else{
            Flushed_list.push_back(check_number);
            return check_number;
        }
        
    }
    
}


