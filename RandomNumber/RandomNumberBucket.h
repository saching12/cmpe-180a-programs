//gid 25
#ifndef RANDOMNUMBERBUCKET_H
#define RANDOMNUMBERBUCKET_H
#include<vector>
using namespace std;

class RandomNumberBucket{
    
public:
    
    RandomNumberBucket();
    
    RandomNumberBucket(int range);
    
    int pop(); // return -1 if empty
    int popWithRefill();
    void refill();
    
    int size(); // Number of elements left
    
    bool empty(); // whether empty or not
    
private:
    
    int check_repeat(int check_number); // whether empty or not
    vector<int> Flushed_list;
    int max_number;
    // Whatever you deem necessary
    
};
#endif
