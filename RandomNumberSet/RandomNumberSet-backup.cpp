#include<vector>
#include<string>
#include<iostream>
#include<sstream>
#include<iomanip>
#include<bitset>
#include<cmath>

using namespace std;

class RandomNumberSet{
    
public:
    RandomNumberSet(){
        maxSize = 39;
        
    }
    
    
    RandomNumberSet(int n){
        
        if(n <= 5){
            cerr << "Entered range is less than 5";
            exit(0);
        }
        else{
               maxSize = n;
           }
    }
    
    
    
    bool set(int i){
        
        bitset<8> num(i);
        if((0 < i) && (i < maxSize)){
            if(bit_number.size()<5)
            {    for(int i =0;i<bit_number.size();i++){
                    if(bit_number[i] == num)
                    return false;
                }
                bit_number.push_back(num);
                return true;
            }
            else{
                cerr << "5 bit set numbers are already stored clear before storing agains";
                exit(0);
            }
        }
        else{
            cerr << "inout I is out of the range!!! Try again"<<endl;
            exit(0);
        }
    }
    
    
    
    void reset(){
        bit_number.clear();
    }
    
    
    
    
    int size(){
        
        return bit_number.size();
        
    }
    
    
    
    
    friend ostream& operator<<(ostream& os,const RandomNumberSet& bit){
        for(int i = 0;i<bit.bit_number.size();i++){
            os << setfill(' ')<< setw(3) <<(int)(bit.bit_number[i].to_ulong())<<endl;
        }
        return os;
    }
    
    
    
    
    int operator-(const RandomNumberSet& bit){
        int i = 0;
    
        int diff = 0;
        diff = abs((int)bit_number.size() - (int)bit.bit_number.size());
        
        if(comparator(bit.bit_number[i]))
            diff++;
        i++;
        if(i < bit.bit_number.size())
           if(comparator(bit.bit_number[i]))
               diff++;
        i++;
        if(i < bit.bit_number.size())
            if(comparator(bit.bit_number[i]))
                diff++;
        i++;
        if(i < bit.bit_number.size())
            if(comparator(bit.bit_number[i]))
                diff++;
        i++;
        if(i < bit.bit_number.size())
            if(comparator(bit.bit_number[i]))
                diff++;
        
        return diff;
        
    }
    
    
private:
    vector<bitset<8> > bit_number;
    int maxSize;
    
    bool comparator(bitset<8> var){
        int j = 0;
        
        if(var != bit_number[j])
        {
            j++;
            if(j < bit_number.size())
                if(var != bit_number[j])
                {
                    j++;
                    if(j < bit_number.size())
                        if(var != bit_number[j])
                        {
                            j++;
        
                            if(j < bit_number.size())
                                if(var != bit_number[j])
                                {
                                    j++;
        
                                    if(j < bit_number.size())
                                        if(var != bit_number[j])
                                            return true;
                                }}}}
        return false;
    }
    
};



int main(){
    
    
    RandomNumberSet num(70),num2;
    
    cout << num.set(5) << endl;
    cout << num.set(15) << endl;
    cout << num.set(13) << endl;
    cout << num.set(12) << endl;
    cout << num.set(5) << endl;
    cout << num.set(14) << endl;
    cout << num;
    num.reset();
    cout << num.set(13) << endl;
    cout << num.set(5) << endl;
    cout << num.set(17) << endl;
    cout << num.set(2) << endl;
    cout << num.size();
    cout << num;
    
//    cout << num2.set(5) << endl;
//    cout << num2.set(15) << endl;
//    cout << num2.set(13) << endl;
//    cout << num2.set(12) << endl;
//    cout << num2.set(5) << endl;
//    cout << "difference" << num-num2 << endl;
    
    
    
    
    
}
