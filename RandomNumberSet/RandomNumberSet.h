#ifndef RANDOMNUMBERSET_H
#define RANDOMNUMBERSET_H
#include<bitset>


using namespace std;

class RandomNumberSet{
    friend ostream& operator<<(ostream& os,const RandomNumberSet& bit);
public:
    RandomNumberSet();
    
    RandomNumberSet(int n);
    
    bool set(int i);
    
    void reset();
    
    int size();
    
    
    
    int operator-(RandomNumberSet& bit);
    
    
private:
    bitset<150>  bit_number;
    int maxSize;
    
    
    
};

#endif
