#include<vector>
#include<string>
#include<iostream>
#include<sstream>
#include<iomanip>
#include<bitset>
#include<cmath>
#include "RandomNumberSet.h"


RandomNumberSet::RandomNumberSet(){
        maxSize = 39;
        
    }
    
    RandomNumberSet::RandomNumberSet(int n){
        
        if(n <= 5){
            cerr << "Entered range is less than 5";
            exit(0);
        }
        else{
               maxSize = n;
           }
}
    
    
    
bool RandomNumberSet::set(int i){
        
        
if((0 < i) && (i < maxSize)){
    
    if(bit_number.count()< 5){
        if(bit_number[i] == 1){
            return false;
        }
        else{
            bit_number.set(i,1);
            return true;
        }
        
    }
    else{
        cerr << "5 bit set numbers are already stored clear before storing agains";
        exit(0);
    }
    
}
else{
    cerr << "inout I is out of the range!!! Try again"<<endl;
    exit(0);
        }
}
    
    
    
void RandomNumberSet::reset(){
    bit_number.reset();
}




int RandomNumberSet::size(){
    
    return bit_number.count();
    
}




ostream& operator<<(ostream& os,const RandomNumberSet& bit){

    for(int i = 0;i<(int)bit.bit_number.size();i++){
        if(bit.bit_number.test(i))
            os << setfill(' ')<< setw(3) <<i;
    }
    os<<endl;
    return os;
}




int RandomNumberSet::operator-(RandomNumberSet& bit){
    
    int x = 0;
    if(bit_number.count() > bit.size())
        x =bit_number.count();
    else
        x = bit.size();
    
    
    return (x - (bit_number&bit.bit_number).count());
    
}



